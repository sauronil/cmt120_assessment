# Exercise 3 - Basic Statistics
def getMedian(l):
    mid = len(l) // 2
    res = (l[mid] + l[-mid-1]) / 2
    if res.is_integer():
        return int(res)
    else: 
        return res
def getAverage(l):
    res=sum(l)/len(l)
    if res.is_integer():
        return int(res)
    else:
        return res
def exercise3(l):
    l.sort()
    sqList=[i**2 for i in l]
    
    t1=(l[0], getAverage(l), getMedian(l), l[len(l) -1])

    t2=(sqList[0], getAverage(sqList), getMedian(sqList), sqList[len(sqList)-1])
    return(t1, t2)



print(exercise3([2,1,4,3,5]))
print(exercise3([7,2,4,5]))
# Hidden Test Cases

print(exercise3([5,3,1,4,2])) #(1,11,9,25)
print(exercise3([2,4,5,7])) #(4,23.5,20.5,49)
