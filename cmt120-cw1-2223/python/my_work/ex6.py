# Exercise 6 - List Depth
def exercise6(l):
    depth=0
    for x in l:
        if isinstance(x, list):
            depth=max(exercise6(x), depth)
    return depth+1


print(exercise6([1,[2,[]],[4,5]]))

print(exercise6([1,2,3]))
print(exercise6([]))
# [(([1,2,3],)  ,1),
# (([1,[2,[]],[4,5]],),3)]