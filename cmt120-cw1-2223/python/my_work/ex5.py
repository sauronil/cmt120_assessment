def exercise5(filename):
    def splitWord(file, punc):
        for i in file:
            if i in punc:
                file=file.replace(i, ' ')
        return file.split()

    
    with open(filename, "r") as f:
        textLines=f.read()
        print(textLines)
        numLetter = numDig = numSym = numWord = numSen = numPara = 0
        punc = ['?','!', '-', ' ', '.', '–' ,',','"', '\'']
        lines=splitWord(textLines, punc)
        numWord+=len(lines)
        para=textLines.split('\n')
        para[:] = [item for item in para if item!='']
        numPara+=len(para)
    for l in textLines:
        print([l])
        numLetter+=l.isalpha()
        numDig+=l.isnumeric()
        numSym+= not(l.isalnum()) and not(l.isspace())
        if l in [".", "?", "!"]:
            numSen+=1

    
    return(numLetter, numDig, numSym, numWord, numSen, numPara)


#print(exercise5("test_data/text1.txt"))
#print(exercise5("test_data/text2.txt"))
print(exercise5("test_data/text3.txt"))

#result (128, 8, 10, 36, 3, 3)

       # punct=["!",'#','$','%', '&','(' ,')', ",", '*', '+' , '-', '/', ':', ';', '<','=', '>', '?', '@', '[', ']', '^', '_', '`', '{', '|', '}', '~', '\n', '\t', " "]