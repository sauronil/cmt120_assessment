def exercise7(amount, coins):
    coinList = [200, 100, 50, 20, 10, 5, 2, 1]
    #coinList = [2, 1, 0.50, 0.20, 0.10, 0.05, 0.02, 0.01]
    amount=amount*100
    j=0
    k=0
    total=0
    def backtrack(coinList, amount, coins, j, k, total):
        if k==coins and total==amount:
            return True
        if j>=len(coinList) or total > amount or k>coins:
            return False

        for i in range(j, len(coinList)):
            if total + coinList[i] <= amount and backtrack(coinList, amount, coins, i, k + 1, total + coinList[i]):
                return True
        return False
    return backtrack(coinList, amount, coins, j, k, total)

print(exercise7(3,2)) #true
print(exercise7(5,2)) #false
print(exercise7(0.3,3)) # True
print(exercise7(1.15,2)) #False 
print(exercise7(5,300)) #True


