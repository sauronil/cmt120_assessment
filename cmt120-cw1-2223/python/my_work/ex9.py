def exercise9(green,yellow,gray):
    with open('test_data/my_wordle.txt', 'r') as f:
        count=0
        for word in f:
            flag = 1
            yellowVals = list(yellow.keys())
            for i in range(5):
                if(word[i] in gray):
                    flag = 0
                    break
                if (i in green) and (word[i] != green[i]):
                    if(word[i] in yellowVals):
                        yellowVals.remove(word[i])
                    break
                if (word[i] in yellow):
                    if(i in yellow[word[i]]):
                        flag=0
                        break
                    else:
                        if(word[i] in yellowVals):
                            yellowVals.remove(word[i])

            if flag and not yellowVals:
                count+=1
    return count

print(exercise9({1:'i',3:'c'}, {'e':{3}}, {'r','a','s','d','f'}))
#print(exercise9({2:'a'}, {'a':{3},'i':{2},'l':{3,4},'r':{1}}, {'e','t','u','o','p','g','h','c','m','s'}))