def exercise7(amount,coins): # sum is amount, coins is coins
    coinList = [200, 100, 50, 20, 10, 5, 2, 1]
    amount = amount * 100
    def coinChange(coinList, amount, coins):
        if amount==0 and coins==0:
            return True
        elif amount==0 and coins>=0:
            return False
        elif amount>0 and coins==0:
            return False
        elif not coinList:
            return False
        coinListCopy=coinList.copy()
        if(max(coinList)>amount):
            coinListCopy.remove(max(coinListCopy))
            return coinChange(coinListCopy, amount, coins)
        else:
            if(not(coinChange(coinListCopy, amount - max(coinList), coins-1))):
                coinListCopy.remove(max(coinListCopy))
                return coinChange(coinListCopy, amount, coins)
            else:
                return True
    return coinChange(coinList, amount, coins)
print(exercise7(3,2)) #true
print(exercise7(5,2)) #false
print(exercise7(0.3,3)) # True
print(exercise7(1.15,2)) #False 