def exercise8(s):
    with open('test_data/wordle.txt', 'r') as f:
        count=0
        for word in f:
            word=word.replace('\n','')
            wordList= list(word)
            strList = list(s)
            for char in word:
                if(char in strList):
                    wordList.remove(char)
                    strList.remove(char)
            if not(wordList):
                count+=1
    return count


print(exercise8('sehouh')) # 1
print(exercise8('caarto')) # 5
print(exercise8('abcde')) # 0
print(exercise8('abcdef')) # 2