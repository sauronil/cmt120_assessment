# exercise2_list = [(('Maltese',9.5,6.7,True, <-- True
# 				 (('Bulldog',16,44,False)] . <-- False

# Exercise 2 - Dog Breeds Standards
def exercise2(breed,height,weight,male):
    stdDict = {('Bulldog', True): (15, 50), ('Bulldog', False): (14, 40),
                ('Dalmatian', True): (24, 70), ('Dalmatian', False): (19, 45),
                ('Maltese', True) : (9, 7), ('Maltese', False): (7,6)}
    stdHeight, stdWeight = stdDict[breed, male]
    if(height<=(stdHeight*1.1) and weight<=(stdWeight*1.1)):
        return True
    return False

print(exercise2('Maltese',9.5,6.7,True))
print(exercise2('Bulldog',16,44,False))

# and (stdHeight >=height*0.9 and stdWeight>=weight*0.9)):

# Hidden Test Cases
print(exercise2('Dalmatian', 18, 49, False)) # True
print(exercise2('Dalmatian', 26, 63, True)) # True