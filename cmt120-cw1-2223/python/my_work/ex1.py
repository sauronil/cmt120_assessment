# Exercise 1 - Iris Species Classifier
# SepalLen=1.5
# SepalWid=0.7
# PetalLen=2
# PetalWid=2.3


def exercise1(SepalLen,SepalWid,PetalLen,PetalWid):
    if (PetalLen < 2.5):
        return "setosa"

    if (PetalWid < 1.8):
        return "versicolor" if (PetalLen < 5) and (PetalWid < 1.7) or PetalLen >= 5 and (PetalWid >= 1.6) and (SepalLen < 7) else "virginica"

    else:
        return "versicolor" if (PetalLen < 4.9) and (SepalLen < 6) else "virginica"


print(exercise1(1.5, 0.7, 2, 2.3))                
print(exercise1(1.9,1.5,2.7,2.5))

# Hidden Cases

print(exercise1(3,2,3.1,1.75)) # virginica
print(exercise1(2,2,5.3,1.6)) # versicolor
print(type(exercise1(2,2,5.3,1.6)))
