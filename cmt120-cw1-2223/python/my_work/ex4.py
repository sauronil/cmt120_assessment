# Exercise 4 - Finite-State Machine Simulator
def exercise4(trans,init_state,input_list):
    outputList=[]
    currState=init_state
    for i in input_list:
        outputVal = trans[currState + "/" + i]        
        x=outputVal.split("/")
        print(x)
        outputList.append(x[1])
        currState=x[0]
    return outputList


print(exercise4({"a/0": "a/1","a/1": "a/0"},'a',['0','0','1','1','0','0'])) # 1 1 0 0 1 1

# Hidden test Cases
print(exercise4({"0/0": "0/None","0/1": "0/None","0/\n": "0/0","1/0": "0/None","1/1": "1/None","1/\n": "1/1"},'1',['1','1','\n'])) # 'None', 'None', ''

