function exercise2(breed, height, weight, male) {
    var stdDict, stdHeight, stdWeight;
    stdDict = { [["Bulldog", true]]: [15, 50], [["Bulldog", false]]: [14, 40], [["Dalmatian", true]]: [24, 70], [["Dalmatian", false]]: [19, 45], [["Maltese", true]]: [9, 7], [["Maltese", false]]: [7, 6] };
    [stdHeight, stdWeight] = stdDict[[breed, male]];
    if (((height <= (stdHeight * 1.1)) && (weight <= (stdWeight * 1.1)))) {
        return true;
    }
    return false;
}
console.log(exercise2("Maltese", 9.5, 6.7, true));
console.log(exercise2("Bulldog", 16, 44, false));