
function exercise6(l){
    let count=1;
    for(let i=0; i<l.length; i++){
        const arr = l[i];
        if(Array.isArray(arr)) {
            count++;
            exercise6(arr);
        }
    }
    return count;
}

// function exercise6(l){
//     function maxDepth (arr){
//         return Array.isArray(arr) ? 1+Math.max(0, ...arr.map(maxDepth)):0;
//     }
//     var max=maxDepth(l);
//     return max;
// }


console.log(exercise6([1, 2, 3]));
console.log(exercise6([1, [2, []], [4, 5]]));