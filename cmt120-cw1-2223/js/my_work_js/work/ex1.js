function exercise1(SepalLen, SepalWid, PetalLen, PetalWid) {
    if ((PetalLen < 2.5)) {
        return "setosa";
    } else {
        if ((PetalWid < 1.8)) {
            if ((PetalLen < 5)) {
                if ((PetalWid < 1.7)) {
                    return "versicolor";
                } else {
                    return "virginica";
                }
            } else {
                if ((PetalWid >= 1.6)) {
                    if ((SepalLen < 7)) {
                        return "versicolor";
                    } else {
                        return "virginica";
                    }
                } else {
                    return "virginica";
                }
            }
        } else {
            if ((PetalLen < 4.9)) {
                if ((SepalLen < 6)) {
                    return "versicolor";
                } else {
                    return "virginica";
                }
            } else {
                return "virginica";
            }
        }
    }
}
console.log(exercise1(1.5, 0.7, 2, 2.3));
console.log(exercise1(1.9, 1.5, 2.7, 2.5));