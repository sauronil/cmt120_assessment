function exercise8(s){
    const fs=require('fs');
    const data=fs.readFileSync('./test_data/wordle.txt', 'utf-8');
    var data_arr=data.split('\n');
    var count =0;
    for(let i=0; i<data_arr.length; i++){
        var temp_word=data_arr[i];
        var wordList=temp_word;
        var strList=s;
        for(let j=0; j<temp_word.length; j++){
            if(strList.includes(temp_word[j])){
                wordList=wordList.replace(temp_word[j], '');
                strList=strList.replace(temp_word[j], '');
            }
        if((wordList === '')) {
            count++; 
        }
        }
    
    }
    return count;
}

console.log(exercise8('sehouh')) // 1
console.log(exercise8('caarto')) // 5
console.log(exercise8('abcde')) // 0
console.log(exercise8('abcdef')) // 2