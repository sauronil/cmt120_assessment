
// Exercise 3 - Basic Statistics
function exercise3(l){
    // Helper Functions 

    function getMedian(l) {
        let mid=Math.floor(l.length/2);
        let median = l.length %2 ===1 ? l[mid]: (l[mid -1] + l[mid])/2;
        if (median.isInteger) {
            return median >>> 0;
        }
        return median;
    }
    
    function getAvg(l){
        let sum=0;
        for(let i =0; i<l.length; i++){
            sum+=l[i];
        }
        let avg=sum/l.length;
        if(sum.isInteger){
            return avg >>> 0;
        }
        return avg;
    }

    // End of Helper Functions

    l.sort(function(a, b){return a - b});
    let sqList=l.map(x=> x*x);
    listA=[l[0], getAvg(l), getMedian(l), l[(l.length)-1]];
    listB=[sqList[0], getAvg(sqList), getMedian(sqList), sqList[(sqList.length)-1]];
    fList= [listA, listB];
    return fList;
}
exercise3([1, 2, 3, 4, 5]);
exercise3([7, 2, 4, 5]);
// { input: [[1, 2, 3, 4, 5]], output: [[1, 3, 3, 5], [1, 11, 9, 25]] },
// { input: [[7, 2, 4, 5]], output: [[2, 4.5, 4.5, 7], [4, 23.5, 20.5, 49]] }