function exercise4 (trans, init_state, input_list) {
    var outputList=[];
    let currState=init_state;
     for(let i=0; i < input_list.length; i++){
        var outputVal = trans[currState + "/" + input_list[i]];
        var x=outputVal.split("/");
        outputList.push(x[1]);
        currState=outputVal[0];
    }
    return outputList;
}

exercise4(
    {"a/0": "a/1","a/1": "a/0"},
            'a',
            ['0', '0', '1', '1', '0', '0']
        );

exercise4(
    {"a/0": "a/1","a/1": "b/0","b/0": "b/0","b/1": "a/1"},
            'a', 
            ['0', '0', '1', '1', '0', '0']
        );

// const EXERCISE4_TESTS = [
//     {
//         input: [{"a/0": "a/1","a/1": "a/0"},
//             'a',
//             ['0', '0', '1', '1', '0', '0']
//         ],
//         output: ['1', '1', '0', '0', '1', '1']
//     },
//     {
//         input: [
//             {"a/0": "a/1","a/1": "b/0","b/0": "b/0","b/1": "a/1"},
//             'a',
//             ['0', '0', '1', '1', '0', '0']
//         ],
//         output: ['1', '1', '0', '1', '1', '1']
// }
// ]