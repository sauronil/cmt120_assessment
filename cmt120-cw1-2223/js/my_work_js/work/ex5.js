const { notEqual, notStrictEqual } = require('assert');

// Exercise 5 - Document Stats
function exercise5(filename) {
    const fs=require('fs');
    const data=fs.readFileSync(filename, 'utf-8');   
    function sanitiseArr(arr) {
        sanitisedArr=[];
        for(let i=0; i<arr.length; i++){
            if(arr[i]){
                sanitisedArr.push(arr[i]);
            }
        }
        return sanitisedArr;
    }
    
    const isAlpha = str => /^[a-zA-Z]*$/.test(str);
    const isDigit = str => /\d/.test(str);
    const isAlnum = str => /^[a-zA-Z0-9]*$/.test(str);
    const isWhiteSpace = str => /\s/.test(str);
    var numLetter=numDig=numSym=numWord=numSen=numPara = 0;
    for(let i=0; i<data.length; i++){
        if(isAlpha(data[i])){
            numLetter++;
        };
        if(isDigit(data[i])){
            numDig++;
        };
        if(!(isAlnum(data[i])) && !(isWhiteSpace(data[i]))) {
            numSym++;
        }
        if(data[i] === '.' || data[i] === '?' || data[i] === '!'){
            numSen++;
        }
    }
    var paraData=data.split('\n\n');
    var sanitisedPara=sanitiseArr(paraData);
    numPara=sanitisedPara.length;
    var wordData=data.split(/[–"-;:'_?.,\s]+/);
    var sanitisedWord=sanitiseArr(wordData);
    numWord=sanitisedWord.length;
    var list=[numLetter, numDig, numSym, numWord, numSen, numPara]
    return list;
}

exercise5('test_data/text3.txt');
//[128, 8, 10, 36, 3, 3]
//84, 0, 3, 19, 1, 4