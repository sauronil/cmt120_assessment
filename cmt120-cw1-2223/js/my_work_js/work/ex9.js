function exercise9(green,yellow,gray) {
    const fs=require('fs');
    const data=fs.readFileSync('../test_data/wordle.txt', 'utf-8');
    var line=data.split('\n');
    var count=0;
    for(let i=0; i<line.length; i++){
        var word=line[i];
        let flag=1;
        let yellow_keys = Object.keys(yellow);
        for(let j=0; j<5; j++){
            if(gray.has(word[j])){
                flag=0;
                break;
            }
            if(green.hasOwnProperty(j) && (word[j] != green[j])){
                flag=0;
                break;
                }
            if(yellow.hasOwnProperty(word[j])){
                let tempSet=yellow[word[j]];
                if(tempSet.has(j)){
                   flag=0;
                   break;
                }
                else {
                    const id = yellow_keys.indexOf(word[j]);
                    if (yellow_keys.includes(word[j])){
                        yellow_keys.splice(id, 1);
                    }
                }

            }
        }
        if(flag===1 && yellow_keys.length===0){
            count++;
        }
    }
    return count;
}

const green_1 = { 1: 'i', 3: 'c' };
const yellow_1 = {'e': new Set([3])};
const gray_1 = new Set(['r', 'a', 's', 'd', 'f']);

const green_2 = {2:'a'}
const yellow_2 = {'a':new Set([3]),'i':new Set([2]),'l':new Set([3,4]),'r':new Set([1])};
const gray_2 = new Set(['e', 't', 'u', 'o', 'p', 'g', 'h', 'c', 'm', 's']);

const green_3 = {};
const yellow_3 = {'r':new Set([1]),'i':new Set([2]),'l':new Set([3])};
const gray_3 = new Set(['g', 'o', 'u', 'p', 'c', 'h']);

const green_4 = { 4: 'r' };
const yellow_4 = {'r':new Set([1]),'i':new Set([1,2]),'l':new Set([0,3])};
const gray_4 = new Set(['g', 'o', 'u', 'p', 'c', 'h', 't', 'e']);

console.log(exercise9(green_1, yellow_1, gray_1)); // 5
console.log(exercise9(green_2, yellow_2, gray_2)); // 3
console.log(exercise9(green_3, yellow_3, gray_3)); // 38
console.log(exercise9(green_4, yellow_4, gray_4)); // 1



