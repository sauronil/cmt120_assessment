function exercise7(amount, coins) {
    const coinList = [200, 100, 50, 20, 10, 5, 2, 1];
    var amount = amount * 100;
    function coinChange(coinList, amount, coins){
        const coinListCopy=[];
        for(let i =0; i<coinList.length; i++){
            coinListCopy.push(coinList[i]);
        }
        if(amount===0 && coins===0){
            return true;
        }
        else if(amount===0 && coins>=0){
            return false;
        }
        else if(amount>0 && coins===0){
            return false;
        }
        else if((coinList.length===0)){
            return false;
        }

        let max = Math.max(...coinList);
        if(max>amount){
            coinListCopy.shift();
            return coinChange(coinListCopy, amount, coins);
        }
        else {
            if(!(coinChange(coinListCopy, amount - max, coins -1))){
                coinListCopy.shift();
                return coinChange(coinListCopy, amount, coins);
            }
            else {
                return true
            }
        }
    }
    return coinChange(coinList, amount, coins);
}

console.log(exercise7(3,2)); // true
console.log(exercise7(5,2)); //false
console.log(exercise7(0.3,3)); // true
console.log(exercise7(1.15,2)); //false
console.log(exercise7(5, 300)); //true
console.log(exercise7(5, 1)); //false
