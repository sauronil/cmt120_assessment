const fs = require('fs');

module.exports = {

    // Exercise 1 - Iris Species Classifier
    exercise1: (SepalLen, SepalWid, PetalLen, PetalWid) => {

    /* Takes in speal length, sepal width, petal length and petal width
        Finding Iris Species by traversing the Decision Tree
        as In Order Traversal for each if - else block 
        Returns: Species as string
    */
        if ((PetalLen < 2.5)) {
            return "setosa";
        } else {
            if ((PetalWid < 1.8)) {
                if ((PetalLen < 5)) {
                    if ((PetalWid < 1.7)) {
                        return "versicolor";
                    } else {
                        return "virginica";
                    }
                } else {
                    if ((PetalWid >= 1.6)) {
                        if ((SepalLen < 7)) {
                            return "versicolor";
                        } else {
                            return "virginica";
                        }
                    } else {
                        return "virginica";
                    }
                }
            } else {
                if ((PetalLen < 4.9)) {
                    if ((SepalLen < 6)) {
                        return "versicolor";
                    } else {
                        return "virginica";
                    }
                } else {
                    return "virginica";
                }
            }
        }    
    
    },

    // Exercise 2 - Dog Breeds Standards
    exercise2: (breed, height, weight, male) => {
        /* Checks if given Breed with height, weight and sex matches
           the Breed Standards with 10% deviation (inclusive)
           Returns: True if it matches; Flase if it does not match 
        */
        var stdDict, stdHeight, stdWeight;
        stdDict = {[["Bulldog", true]]: [15, 50], 
                [["Bulldog", false]]: [14, 40], 
                [["Dalmatian", true]]: [24, 70], 
                [["Dalmatian", false]]: [19, 45], 
                [["Maltese", true]]: [9, 7], 
                [["Maltese", false]]: [7, 6]};

        /* created dictionary to store key value pairs 
           for dog breeds and standard sizes
           True - Male
           False - Female
        */
                
        [stdHeight, stdWeight] = stdDict[[breed, male]]; 
        // getting value from key which is being passed from the function call
        if (((height <= (stdHeight * 1.1)) && (weight <= (stdWeight * 1.1)))) {
            return true; // 1.1 = 10% (inclusive) 
        }
        return false;
    },

    // Exercise 3 - Basic Statistics
    exercise3: (l) => {
       // Start of Helper Functions        
            function getMedian(l) {
                /* Takes a list of integer values
                   Returns - Median of the list
                */
                let mid=Math.floor(l.length/2);
                let median = l.length %2 ===1 ? l[mid]: (l[mid -1] + l[mid])/2;
                if (median.isInteger) { 
                    return median >>> 0; /* bitwise shift operator 
                    to remove trailing decimals upto 3 decimal places
                    test case checks as 3 =/= 3.0
                    */
                }
                return median;
            }
            function getAvg(l){
                /* Takes a list of integer values
                   Returns - average from the list of values
                */
                let sum=0;
                for(let i =0; i<l.length; i++){
                    sum+=l[i];
                }
                let avg=sum/l.length;
                if(sum.isInteger){
                    return avg >>> 0; /* bitwise shift operator 
                    to remove trailing decimals upto 3 decimal places
                    test case checks as 3 =/= 3.0
                    */
                }
                return avg;
            }
            // End of Helper Functions

            l.sort(function(a, b){return a - b}); // sorting list
                                                  // increasing order
            let sqList=l.map(x=> x*x); // deep copy of give list and squaring it
            listA=[l[0], getAvg(l), getMedian(l), l[(l.length)-1]]; 
            listB=[sqList[0], getAvg(sqList), getMedian(sqList), sqList[(sqList.length)-1]];
            return [listA, listB]; // returing a tuple of list
        },

    // Exercise 4 - Finite-State Machine Simulator
    exercise4: (trans, init_state, input_list) => {
        /* Takes in dictionary of transition state, 
           initial state and list of inputs
           Returns - output list
        */
        var outputList=[];
        let currState=init_state;
         for(let i=0; i < input_list.length; i++){
            var outputVal = trans[currState + "/" + input_list[i]];
            // storing value from trans dict || "current state"/"input_list"
            var x=outputVal.split("/"); // splitting it with /
            outputList.push(x[1]); // storing the value after '/' 
                                   // x[1] indicates output
            currState=outputVal[0]; // changing the state to output state
                                    // x[0] indicates state
        }
        return outputList;
    },

    // Exercise 5 - Document Stats
    exercise5: (filename) => {
        /* Takes in a text file from os
        Returns - Number of Letters,
                  Number of Digits,
                  Number of Symbols,
                  Number of Words,
                  Number of Sentences,
                  Number of Paragraphs
        */
        const fs=require('fs');
        const data=fs.readFileSync(filename, 'utf-8');    
        function sanitiseArr(arr) {
            /* Takes in an array
                Removes empty elemets from array
                Returns array with only valid elements
            */

            /* Note: Sanitising becuase of edge case where there is
                multiple new line before a paragraph starts
            */
            var arrSanitised=[];
            for(let i=0; i<arr.length; i++){
                if(arr[i]){ // checking if element exist
                    arrSanitised.push(arr[i]);
                }
            }
            return arrSanitised;
        }

        const isAlpha = str => /^[a-zA-Z]*$/.test(str); 
        // regex to check character is an alphabet
        const isDigit = str => /\d/.test(str); 
        // regex to check character is digit
        const isAlnum = str => /^[a-zA-Z0-9]*$/.test(str); 
        //regex to check if character is alphanumeric
        const isWhiteSpace = str => /\s/.test(str); 
        // regex to check if character is is an whitespace

        // reference: 
        // https://regexr.com/
        // implemented by using own test cases and edge case for this particular
        // exerercise

        var numLetter=numDig=numSym=numWord=numSen=numPara = 0;
        for(let i=0; i<data.length; i++){
            // data[i] indicates character
            if(isAlpha(data[i])){ 
                numLetter++;
            };
            if(isDigit(data[i])){
                numDig++;
            };
            if(!(isAlnum(data[i])) && !(isWhiteSpace(data[i]))) {
                numSym++;
            }
            if(data[i] === '.' || data[i] === '?' || data[i] === '!'){ 
            //checking if character is '.', .!. , '?' 
            //sentence ends with these punctuations
                numSen++;
            }
        }
        var paraData=data.split('\n\n'); 
        // spliting data when it hits a two new lines, 
        // one for new line, next for whitespace
        var sanitisedPara=sanitiseArr(paraData); 
        numPara=sanitisedPara.length; 
        // give number of elements in the Para List
        var wordData=data.split(/[-;:'_–"?.,\s]+/); 
        // splitting data on punctuations (not extensive)
        var sanitisedWord=sanitiseArr(wordData);
        numWord=sanitisedWord.length; 
        // give number of elements in Word List
        return [numLetter, numDig, numSym, numWord, numSen, numPara];
    },

    // Exercise 6 - List Depth
    exercise6: (l) => {
        
        function maxDepth(arr) {
        /* Takes in a List
           Returns - Number of sublists / max depth of a sublist
        */
            let count=0;
            for(let i=0; i<arr.length; i++){
                const newArr = arr[i];
                if(Array.isArray(newArr)) // checking if element is type array 
                {
                    count++; // increased counter
                    maxDepth(newArr);
                    // reccursively checking 
                    // for array in this element(i.e array)
                }
            }
            return count+1; // if given array is empty returns 1
        }
        return maxDepth(l);
    },

    // Exercise 7 - Change, please
    exercise7: (amount,coins) => {
        /* Takes in amount and number of coins
        Returns true if amount can be achieved from given number of coins
        Returns false if cannot
        */
        const coinArr = [200, 100, 50, 20, 10, 5, 2, 1];
        // denomination array
        var amount = amount * 100;
        /*Multiplied denomination with 100 such as 
        2 pounds to 200, 1 pound to 100 and so on for 
        Simpler integer calculations
        Note: Floating point calculations takes a toll in CPU resources
        */
        function backTrack(coinArr, amount, coins){
            /* Takes in denomination of coins, amount, coins
            in each all checks if coin = amount = 0 = success
            if not change denomination with a lower value, resets 
            coin & amount and continues to check untill 
            denomination list has been exhausted.
            Returns - true if amount can be made with given number of coins
            false if amount cannot be reached with given number of coins
            */
            
            const coinArrTemp=[];
            for(let i =0; i<coinArr.length; i++){
                coinArrTemp.push(coinArr[i]);
            }
            /* creating deep copy of denomination to remove biggest 
             denomination from array without losing the actual array
            */
            if(amount===0 && coins===0){
                return true;
            }
            else if(amount===0 && coins>=0){
                return false;
            }
            else if(amount>0 && coins===0){
                return false;
            }
            else if((coinArr.length===0)){
                return false;
            }
            

            let max = Math.max(...coinArr); // getting the max denomination
            if(max>amount){
                coinArrTemp.shift(); // removing biggest denomination
                return backTrack(coinArrTemp, amount, coins);
            }
            else {
                if(!(backTrack(coinArrTemp, amount - max, coins -1))){
                    coinArrTemp.shift(); // removing denomination
                    return backTrack(coinArrTemp, amount, coins);
                }
                else {
                    return true
                }
            }
        }
        return backTrack(coinArr, amount, coins);
    },

    // Exercise 8 - Five Letter Unscramble
    exercise8: (s) => {
        /* Takes in a string of characters
        Returns number of words can be make from the given string 
        from the wordle.txt(contains list of meaningful words) 
        */
       
        const fs=require('fs');
        const data=fs.readFileSync('./test_data/wordle.txt', 'utf-8');
        var datArr=data.split('\n'); //split the file into list of words
        var count=0; // counter for possible words
        for(let i=0; i<datArr.length; i++){ // for word in file
            var charWord=datArr[i];
            var wordList=charWord;
            var strList=s;
            for(let j=0; j<charWord.length; j++){ //for character in word
                if(strList.includes(charWord[j])){ 
                    // if character exists in word from list given of letters
                    wordList=wordList.replace(charWord[j], ''); 
                    // remove char from word
                    strList=strList.replace(charWord[j], ''); 
                    // remove char from list of letters
                }
            if((wordList === '')) { // if wordlist is empty
                count++; // word exists 
            }
            }    
        }
        return count; // return number of word exists in wordle.txt
        // Note - Code can be better. Time Complexity is O(n^2)
    },  

    // Exercise 9 - Wordle Set
    exercise9: (green,yellow,gray) => {
        /* Takes in dictionary of green which contains character index 
            and character, yellow is a dictionary which contains character and
            a set which tells us index and gray is set of characters
            Returns - Number of possible words 
        */
        const fs=require('fs');
        const data=fs.readFileSync('./test_data/wordle.txt', 'utf-8');
        var line=data.split('\n'); 
        var count=0; // setting counter
        for(let i=0; i<line.length; i++){
            var word=line[i];
            let flag=1;
            let yellow_keys = Object.keys(yellow); // array of character from 
                                                   // yellow set for the 
                                                   // current word
            for(let j=0; j<5; j++){
                if(gray.has(word[j])){ // character is in gray set, break
                    flag=0;
                    break;
                }
                if(green.hasOwnProperty(j) && (word[j] != green[j])){
                    // chracter is in index of green and not in position
                    flag=0;
                    break;
                    }
                if(yellow.hasOwnProperty(word[j])){
                    // if character in yellow
                    let tempSet=yellow[word[j]];
                    if(tempSet.has(j)){
                        // if character in position sets
                    flag=0;
                    break;
                    }
                    else {
                        const id = yellow_keys.indexOf(word[j]);
                        if (yellow_keys.includes(word[j])){
                            // characters in yellow dict 
                            // are present in word but not in position
                            // in the set for the character
                            yellow_keys.splice(id, 1);
                        }
                    }
                }
            }
            if(flag===1 && yellow_keys.length===0){
                count++; // word matches all criteria
            }
        }
        return count;
    },

    // Exercise 10 - One Step of Wordle
    exercise10: (green,yellow,gray) => {
        return undefined;
    },
}
