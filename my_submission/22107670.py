import copy

# Exercise 1 - Iris Species Classifier

def exercise1(SepalLen,SepalWid,PetalLen,PetalWid):    
    """ Finding Iris Species by traversing the Decision Tree
        as In Order Traversal for each if - else block 
        Returns: Species as string
    """

    if (PetalLen < 2.5):
        return "setosa"
    if (PetalWid < 1.8):
        if (PetalLen < 5):
            return "versicolor" if (PetalWid < 1.7) else "virginica"
        if (PetalWid >=1.6):
            return "versicolor" if (SepalLen < 7) else "virginica"
        return "virginica"
    if (PetalLen < 4.9):
        return "versicolor" if (SepalLen < 6) else "virginica"
    return "virginica"

# Exercise 2 - Dog Breeds Standards
def exercise2(breed,height,weight,male):
    """ Checks if given Breed with height, weight and sex matches
        the Breed Standards with 10% deviation (inclusive)
        Returns: True if it matches; Flase if it does not match
    """
    stdDict = {('Bulldog', True): (15, 50), ('Bulldog', False): (14, 40),
                ('Dalmatian', True): (24, 70), ('Dalmatian', False): (19, 45),
                ('Maltese', True) : (9, 7), ('Maltese', False): (7,6)}

    """ created dictionary to store key value pairs 
    for dog breeds and standard sizes
    # True - Male
    # False - Female
    """
    stdHeight, stdWeight = stdDict[breed, male]
    return height<=(stdHeight*1.1) \
    and weight<=(stdWeight*1.1) #1.1 = 10% (inclusive)


# Exercise 3 - Basic Statistics
def exercise3(l):
    #Start of Helper Functions
    
    def getMedian(l):
        """ Takes a list of integer values
            Returns - Median of the list 
        """
        mid = len(l) // 2
        res = (l[mid] + l[-mid-1]) / 2
        return int(res) if res.is_integer() else res 
        # is_integer - test case checks 3 =/= 3.0
        # returning abosulute value
 
    def getAverage(l):
        """ Takes a list of integer values
            Returns - average from the list of values
        """       
        res=sum(l)/len(l)
        return int(res) if res.is_integer() else res
        # is_integer - test case checks 3 =/= 3.0
        # returning abosulute value

    # End of Helper Functions

    l.sort() # sorting list from lowest to highest 
    sqList=[i**2 for i in l] # created a copy of the list 
                             # and squared each value
    stat1=(l[0], getAverage(l), getMedian(l), l[len(l) -1])
    stat2 = sqList[0], getAverage(sqList), getMedian(sqList), sqList[-1]
    return [stat1, stat2] 
    # returing as tuples of list

# Exercise 4 - Finite-State Machine Simulator
def exercise4(trans,init_state,input_list):
    """ Takes in dictionary of transition state, 
        initial state and list of inputs
        Returns - output list
    """
    outputList=[]
    currState=init_state
    for i in input_list:
        outputVal = trans[f"{currState}/{i}"] # using string concatenation
        x=outputVal.split("/") #spliting values as (state, output)
        outputList.append(x[1]) # storing output value in list 
                                # and x[1] indicates output
        currState=x[0] # changing state to output state and 
                       # x[0] indicates state
    return outputList

# Exercise 5 - Document Stats
def exercise5(filename):
    """ Takes in a text file from os
        Returns - Number of Letters,
                  Number of Digits,
                  Number of Symbols,
                  Number of Words,
                  Number of Sentences,
                  Number of Paragraphs
    """
    def splitWord(lines, punc):
        """ Takes in lines and a list of punctuations
            splits the line into words with punctuation as delimeter
            and removes all punctuations from the word
            Returns - words with no punctuations
        """
        for i in lines:
            if i in punc:
                lines=lines.replace(i, ' ') # Replacing punctuations 
                                            # characters with empty
        return lines.split()
    # Reading File
    with open(filename, "r", encoding='utf-8') as f:
        textLines=f.read() #read file line by line
        numLetter = numDig = numSym = numWord = numSen = numPara = 0
        punc = ['!','–' ,'-', ' ', '.', ':', ';','"',',', '\''] 
        # Punctuation List (Not Extensive)
        
        lines=splitWord(textLines, punc)
        numWord+=len(lines)
        para=textLines.split('\n\n') # Splitting elements with new 
                                     # line and whitespace
        para[:] = [item for item in para if item!=''] # Removing empty element
                                                      # from list
        numPara+=len(para)
    for l in textLines: # reading character by by character from line
        numLetter+=l.isalpha()
        numDig+=l.isnumeric()
        numSym+= not(l.isalnum()) and not(l.isspace())
        if l in [".", "?", "!"]: # Sentences end with '.', '?', '!'
            numSen+=1
    return(numLetter, numDig, numSym, numWord, numSen, numPara)


# Exercise 6 - List Depth
def exercise6(l):
    def maxDepth(myList):
        """ Takes in a List
            Returns - Number of sublists / max depth of a sublist
        """
        depth=0
        for i in myList:
            if isinstance(i, list): # checking if element in the list 
                                    # is type(list)
                depth=max(maxDepth(i), depth) # recurively calls maxDepth 
                                              # and stores the max value
                                              # from the function call
        return depth+1 # if empty list returns (0 + 1 =1) 
    return maxDepth(l)

# Exercise 7 - Change, please
def exercise7(amount,coins):
    """ Takes in amount and number of coins
        Returns true if amount can be achieved from given number of coins
        Returns false if cannot
    """
    coinList = [200, 100, 50, 20, 10, 5, 2, 1] # denominations list
    # Multiplied denomination with 100 such as 2 pounds to 200 and so on for 
    # simpler integer calculations
    # Note: Floating point calculations takes a toll in CPU resources
    
    amount=amount*100
    j=0 #counter for coinList
    k=0 #counter for coins
    total=0 # counter for amount

    def backtrack(coinList, amount, coins, j, k, total):
        """ takes in denomination of coins, amount, coins, j, k and total
            in each all checks if coin = amount = 0 = success
            if not change denomination with a lower value, 
            increase counter which checks coins & amount and
            continues to check untill denomination list has been exhausted
            Returns - true if amount can be made with given number of coins
            false if amount cannot be reached with given number of coins
        """
        # early exit
        if k==coins and total==amount:
            return True
        if j>=len(coinList) or total>amount or k>coins:
            return False
        # check begins here
        for i in range(j, len(coinList)):
            if total+coinList[i]<=amount and \
                backtrack(coinList, amount, coins, i, k+1, total+coinList[i]):
                return True
        return False
    return backtrack(coinList, amount, coins, j, k, total)

# Exercise 8 - Five Letter Unscramble
def exercise8(s):
    """ Takes in a string of characters
        Returns number of words can be make from the given string 
        from the wordle.txt(contains list of meaningful words)
    """
    with open('./test_data/wordle.txt', 'r') as f:
        count=0 # setting count possible words
        for word in f:
            word=word.replace('\n','') #removing new line char, \n causes bugs
            wordList= list(word) # converting word to list of characters
                                 # since strings are immutable
            strList = list(s) # same logic as above
            for char in word:
                if(char in strList): # checking if character is in strList
                    wordList.remove(char)
                    strList.remove(char)
                    # if true remove the character from word list and str list
            if not(wordList): # if wordlist gets empty, 
                              # it means we have found a word from file
                count+=1 # increase counter
    return count

# Exercise 9 - Wordle Set
def exercise9(green,yellow,gray):
    """ Takes in green as dictionary, yellow as dictionary with key as char
        and values as set for character index and gray as a set
        Returns - Number of words in wordle.txt which satisfies 
        the given constraints 
    """
    with open('./test_data/wordle.txt', 'r') as f:
        count=0 # counter
        for word in f:
            flag = 1 # setting flag for checking if all condition is true
            yellowVals = list(yellow.keys()) # list of yellow values(i.e set)
            for i in range(5):
                if(word[i] in gray): #character in gray set
                    flag = 0
                    break
                if (i in green) and (word[i] != green[i]): 
                # key in green dict and character not in position
                    if(word[i] in yellowVals):
                    # character in yellow list
                        yellowVals.remove(word[i])
                        # remove character from yellow list
                    flag=0
                    break
                if (word[i] in yellow):
                # character in yellow key
                    if(i in yellow[word[i]]):
                    # number in yellow character
                        flag=0
                        break
                    else:
                        if(word[i] in yellowVals):
                        # character in yellow set
                            yellowVals.remove(word[i])

            if flag and not yellowVals:
            # if flag true and yellowVals is empty
                count+=1 # increase counter, since word matches all conditions
    return count

# Exercise 10 - One Step of Wordle
def exercise10(green,yellow,gray):
    return None